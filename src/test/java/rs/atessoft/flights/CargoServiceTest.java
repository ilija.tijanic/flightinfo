package rs.atessoft.flights;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.domain.WeightUnit;
import rs.atessoft.flights.entity.Cargo;
import rs.atessoft.flights.repository.CargoRepository;
import rs.atessoft.flights.service.impl.CargoServiceImpl;

import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CargoServiceTest {

    @Mock
    private CargoRepository cargoRepository;

    @InjectMocks
    private CargoServiceImpl cargoService;

    @Test
    public void whenNoCargoForFlight_thenZeroReturned() {
        long flightId = 1L;
        CargoType cargoType = CargoType.CARGO;

        Mockito.when(cargoRepository.findAllByFlightIdAndType(flightId, cargoType)).thenReturn(Collections.emptyList());

        Double expected = 0d;
        Double actual = cargoService.calculateWeight(flightId, cargoType, WeightUnit.KG);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void whenWeightUnitIsLb_thenConvertedWeightReturned() {
        long flightId = 1L;
        CargoType cargoType = CargoType.CARGO;

        List<Cargo> cargoList = List.of(
                Cargo.builder()
                        .pieces(10d)
                        .type(cargoType)
                        .weight(10d)
                        .weightUnit(WeightUnit.LB)
                        .build(),
                Cargo.builder()
                        .pieces(10d)
                        .type(cargoType)
                        .weight(10d)
                        .weightUnit(WeightUnit.KG)
                        .build()
        );

        Mockito.when(cargoRepository.findAllByFlightIdAndType(flightId, cargoType)).thenReturn(cargoList);

        Double expected = 32.0462;
        Double actual = cargoService.calculateWeight(flightId, cargoType, WeightUnit.LB);

        Assertions.assertEquals(expected, actual);
    }

}
