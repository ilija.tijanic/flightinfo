package rs.atessoft.flights.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.domain.WeightUnit;
import rs.atessoft.flights.entity.Cargo;
import rs.atessoft.flights.repository.CargoRepository;
import rs.atessoft.flights.service.CargoService;

@Service
@RequiredArgsConstructor
public class CargoServiceImpl implements CargoService {

	private final CargoRepository cargoRepository;

	@Override
	public Double calculateWeight(Long flightId, CargoType type, WeightUnit neededUnit) {
		return cargoRepository.findAllByFlightIdAndType(flightId, type)
				.parallelStream()
				.map(cargo -> calculateItemWeight(cargo, neededUnit))
				.reduce(0d, Double::sum);
	}

	private Double calculateItemWeight(Cargo cargo, WeightUnit neededUnit) {
		return cargo.getWeightUnit().convert(cargo.getWeight(), neededUnit);
	}

}
