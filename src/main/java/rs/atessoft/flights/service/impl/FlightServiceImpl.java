package rs.atessoft.flights.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.domain.WeightUnit;
import rs.atessoft.flights.dto.AirportDailyInfoResponse;
import rs.atessoft.flights.dto.AirportInfo;
import rs.atessoft.flights.dto.FlightWeightResponse;
import rs.atessoft.flights.repository.FlightRepository;
import rs.atessoft.flights.service.CargoService;
import rs.atessoft.flights.service.FlightService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;
    private final CargoService cargoService;

    @Override
    public List<FlightWeightResponse> getFlightWeightInfo(int flightNumber, LocalDate date, WeightUnit unit) {
        return flightRepository.findByFlightNumberAndDate(flightNumber, date)
                .parallelStream()
                .map(flight -> {
                    Double baggageWeight = cargoService.calculateWeight(flight.getFlightId(), CargoType.BAGGAGE, unit);
                    Double cargoWeight = cargoService.calculateWeight(flight.getFlightId(), CargoType.CARGO, unit);
                    return FlightWeightResponse.builder()
                            .flightId(flight.getFlightId())
                            .baggageWeight(baggageWeight)
                            .cargoWeight(cargoWeight)
                            .totalWeight(baggageWeight + cargoWeight)
                            .build();
                })
                .collect(Collectors.toList());
    }

    @Override
    public AirportDailyInfoResponse getAirportFlightInfo(String airportCode, LocalDate date) {
        AirportInfo arrivingFlights = flightRepository.findBaggageAndFlightsCountByArrivalAirportCodeAndDate(airportCode, date);
        AirportInfo departingFlights = flightRepository.findBaggageAndFlightsCountByDepartureAirportCodeAndDate(airportCode, date);
        return AirportDailyInfoResponse.builder()
                .arrivingFlights(arrivingFlights.getNumberOfFlights().orElse(0))
                .departingFlights(departingFlights.getNumberOfFlights().orElse(0))
                .arrivingBaggage(arrivingFlights.getNumberOfBaggage().orElse(0))
                .departingBaggage(departingFlights.getNumberOfBaggage().orElse(0))
                .build();
    }

}
