package rs.atessoft.flights.service;

import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.domain.WeightUnit;

public interface CargoService {

	Double calculateWeight(Long flightId, CargoType type, WeightUnit unit);

}
