package rs.atessoft.flights.service;

import rs.atessoft.flights.domain.WeightUnit;
import rs.atessoft.flights.dto.AirportDailyInfoResponse;
import rs.atessoft.flights.dto.FlightWeightResponse;

import java.time.LocalDate;
import java.util.List;

public interface FlightService {

    List<FlightWeightResponse> getFlightWeightInfo(int flightNumber, LocalDate date, WeightUnit unit);

    AirportDailyInfoResponse getAirportFlightInfo(String airportCode, LocalDate date);

}
