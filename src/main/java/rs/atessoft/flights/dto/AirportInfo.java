package rs.atessoft.flights.dto;

import java.util.Optional;

public interface AirportInfo {
	Optional<Integer> getNumberOfFlights();

	Optional<Integer> getNumberOfBaggage();
}
