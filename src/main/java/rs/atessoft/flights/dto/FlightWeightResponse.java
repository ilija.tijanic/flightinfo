package rs.atessoft.flights.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightWeightResponse {

	private Long flightId;
	private Double cargoWeight;
	private Double baggageWeight;
	private Double totalWeight;

}
