package rs.atessoft.flights.dto;

import lombok.Data;
import rs.atessoft.flights.domain.WeightUnit;

import java.util.List;

@Data
public class CargoDto {

	private Long flightId;
	private List<Item> baggage;
	private List<Item> cargo;

	@Data
	public static class Item {
		private Long id;
		private Double weight;
		private WeightUnit weightUnit;
		private Double pieces;
	}

}
