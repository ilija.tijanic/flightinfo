package rs.atessoft.flights.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirportDailyInfoResponse {

	private int departingFlights;
	private int arrivingFlights;
	private int departingBaggage;
	private int arrivingBaggage;

}
