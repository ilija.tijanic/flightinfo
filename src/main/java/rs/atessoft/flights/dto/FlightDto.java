package rs.atessoft.flights.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class FlightDto {

	private Long flightId;
	private int flightNumber;
	private String departureAirportIATACode;
	private String arrivalAirportIATACode;
	private OffsetDateTime departureDate;

}
