package rs.atessoft.flights.init;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import rs.atessoft.flights.dto.CargoDto;
import rs.atessoft.flights.dto.FlightDto;
import rs.atessoft.flights.entity.Cargo;
import rs.atessoft.flights.entity.Flight;
import rs.atessoft.flights.mapper.CargoMapper;
import rs.atessoft.flights.mapper.FlightMapper;
import rs.atessoft.flights.repository.TypedRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataInitializer {

	private final List<TypedRepository<?>> repositories;
	private final ResourceLoader resourceLoader;
	private final ObjectMapper objectMapper;

	@EventListener(ApplicationReadyEvent.class)
	public void initData() throws IOException {
		loadData(Flight.class, FlightDto.class, FlightMapper::map);
		loadData(Cargo.class, CargoDto.class, CargoMapper::map);
	}

	private <T, R> void loadData(Class<T> entityClass, Class<R> dtoClass, Function<List<R>, List<T>> mapFunction) throws IOException {
		String entityClassName = entityClass.getSimpleName();
		String fileLocation = getFileLocation(entityClassName);
		InputStream inputStream = resourceLoader.getResource(fileLocation).getInputStream();
		CollectionType valueType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, dtoClass);
		List<R> dtoList = objectMapper.readValue(inputStream, valueType);
		List<T> entityList = mapFunction.apply(dtoList);
		findRepository(entityClass).ifPresent(repository -> {
			repository.saveAll(entityList);
			log.info("Saved {} entities: {}", entityClassName, entityList);
		});
	}

	@SuppressWarnings("unchecked")
	private <T> Optional<TypedRepository<T>> findRepository(Class<T> clazz) {
		return repositories.stream()
				.filter(repository -> repository.getTypeClass().equals(clazz))
				.map(repository -> (TypedRepository<T>) repository)
				.findFirst();
	}

	private String getFileLocation(String entityClassName) {
		return "classpath:" + entityClassName + ".json";
	}

}
