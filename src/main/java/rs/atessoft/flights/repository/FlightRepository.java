package rs.atessoft.flights.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.atessoft.flights.dto.AirportInfo;
import rs.atessoft.flights.entity.Flight;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FlightRepository extends TypedRepository<Flight> {

	@Query("select f from Flight f where f.flightNumber = :flightNumber and f.departureDate "
			+ "between :#{#date.atStartOfDay()} and :#{#date.atTime(23, 59, 59, 999)}")
	List<Flight> findByFlightNumberAndDate(int flightNumber, LocalDate date);

	@Query("select count(distinct f.flightId) as numberOfFlights, sum(c.pieces) as numberOfBaggage "
			+ "from Flight f join Cargo c on c.flightId = f.flightId "
			+ "where f.arrivalAirportIATACode = :airportCode and f.departureDate "
			+ "between :#{#date.atStartOfDay()} and :#{#date.atTime(23, 59, 59, 999)} "
			+ "and c.type = 'BAGGAGE'")
	AirportInfo findBaggageAndFlightsCountByArrivalAirportCodeAndDate(String airportCode, LocalDate date);

	@Query("select count(distinct f.flightId) as numberOfFlights, sum(c.pieces) as numberOfBaggage "
			+ "from Flight f join Cargo c on c.flightId = f.flightId "
			+ "where f.departureAirportIATACode = :airportCode and f.departureDate "
			+ "between :#{#date.atStartOfDay()} and :#{#date.atTime(23, 59, 59, 999)} "
			+ "and c.type = 'BAGGAGE'")
	AirportInfo findBaggageAndFlightsCountByDepartureAirportCodeAndDate(String airportCode, LocalDate date);

	@Override
	default Class<Flight> getTypeClass() {
		return Flight.class;
	}

}
