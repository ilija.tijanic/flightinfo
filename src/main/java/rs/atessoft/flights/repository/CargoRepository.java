package rs.atessoft.flights.repository;

import org.springframework.stereotype.Repository;
import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.entity.Cargo;

import java.util.List;

@Repository
public interface CargoRepository extends TypedRepository<Cargo> {

	List<Cargo> findAllByFlightIdAndType(Long flightId, CargoType type);

	@Override
	default Class<Cargo> getTypeClass() {
		return Cargo.class;
	}

}
