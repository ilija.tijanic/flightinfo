package rs.atessoft.flights.converter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import rs.atessoft.flights.domain.WeightUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LBConverter extends WeightUnitConverter {

	private static LBConverter instance;

	public static LBConverter getInstance() {
		if (instance == null) {
			instance = new LBConverter();
		}
		return instance;
	}

	@Override
	void initFunctions() {
		instance.addConverter(WeightUnit.LB, weight -> weight);
		instance.addConverter(WeightUnit.KG, weight -> weight * 0.453592);
	}
}
