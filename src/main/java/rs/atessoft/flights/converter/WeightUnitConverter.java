package rs.atessoft.flights.converter;

import rs.atessoft.flights.domain.WeightUnit;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class WeightUnitConverter {

	private final Map<WeightUnit, Function<Double, Double>> map = new HashMap<>();

	void addConverter(WeightUnit unit, Function<Double, Double> function) {
		map.put(unit, function);
	}

	public Function<Double, Double> getConvertFunction(WeightUnit unit) {
		if (map.isEmpty()) {
			initFunctions();
		}
		return map.get(unit);
	}

	abstract void initFunctions();

}
