package rs.atessoft.flights.converter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import rs.atessoft.flights.domain.WeightUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KGConverter extends WeightUnitConverter {

	private static KGConverter instance;

	public static KGConverter getInstance() {
		if (instance == null) {
			instance = new KGConverter();
		}
		return instance;
	}

	@Override
	void initFunctions() {
		instance.addConverter(WeightUnit.KG, weight -> weight);
		instance.addConverter(WeightUnit.LB, weight -> weight * 2.20462);
	}
}
