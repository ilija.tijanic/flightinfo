package rs.atessoft.flights.domain;

public enum CargoType {
	CARGO, BAGGAGE
}
