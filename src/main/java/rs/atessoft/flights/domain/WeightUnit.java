package rs.atessoft.flights.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import rs.atessoft.flights.converter.KGConverter;
import rs.atessoft.flights.converter.LBConverter;
import rs.atessoft.flights.converter.WeightUnitConverter;

@RequiredArgsConstructor
public enum WeightUnit {

	@JsonProperty("kg")
	KG(KGConverter.getInstance()),
	@JsonProperty("lb")
	LB(LBConverter.getInstance());

	private final WeightUnitConverter converter;

	public Double convert(Double weight, WeightUnit neededUnit) {
		return converter.getConvertFunction(neededUnit).apply(weight);
	}

}
