package rs.atessoft.flights.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.atessoft.flights.domain.WeightUnit;
import rs.atessoft.flights.dto.AirportDailyInfoResponse;
import rs.atessoft.flights.dto.FlightWeightResponse;
import rs.atessoft.flights.service.FlightService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/flight-info")
public class FlightInfoController {

    private final FlightService flightService;

    @GetMapping("/weight")
    public ResponseEntity<List<FlightWeightResponse>> getFlightWeightInfo(@RequestParam int flightNumber,
                                                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                                                          @RequestParam LocalDate date,
                                                                          @RequestParam(required = false, defaultValue = "KG")
                                                                                  WeightUnit unit) {
        return ResponseEntity.ok(flightService.getFlightWeightInfo(flightNumber, date, unit));
    }

    @GetMapping("/airport")
    public ResponseEntity<AirportDailyInfoResponse> getAirportFlightInfo(@RequestParam String airportCode,
                                                                         @DateTimeFormat(pattern = "yyyy-MM-dd")
                                                                         @RequestParam LocalDate date) {
        return ResponseEntity.ok(flightService.getAirportFlightInfo(airportCode, date));
    }

}

