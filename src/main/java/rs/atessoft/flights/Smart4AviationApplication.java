package rs.atessoft.flights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Smart4AviationApplication {

	public static void main(String[] args) {
		SpringApplication.run(Smart4AviationApplication.class, args);
	}

}
