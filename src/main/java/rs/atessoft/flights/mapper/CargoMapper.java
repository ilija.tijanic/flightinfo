package rs.atessoft.flights.mapper;

import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.dto.CargoDto;
import rs.atessoft.flights.entity.Cargo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CargoMapper {

	public static List<Cargo> map(List<CargoDto> dtoList) {
		return dtoList.stream()
				.flatMap(CargoMapper::map)
				.collect(Collectors.toList());
	}

	public static Stream<Cargo> map(CargoDto dto) {
		List<Cargo> cargoList = new ArrayList<>();

		addItems(dto.getBaggage(), cargoList, dto.getFlightId(), CargoType.BAGGAGE);
		addItems(dto.getCargo(), cargoList, dto.getFlightId(), CargoType.CARGO);

		return cargoList.stream();
	}

	private static void addItems(List<CargoDto.Item> items, List<Cargo> cargoList, Long flightId, CargoType type) {
		items.stream()
				.map(item -> CargoMapper.map(item, flightId, type))
				.forEach(cargoList::add);
	}

	private static Cargo map(CargoDto.Item item, Long flightId, CargoType type) {
		return Cargo.builder()
				.flightId(flightId)
				.id(item.getId())
				.weight(item.getWeight())
				.weightUnit(item.getWeightUnit())
				.pieces(item.getPieces())
				.type(type)
				.build();
	}

}
