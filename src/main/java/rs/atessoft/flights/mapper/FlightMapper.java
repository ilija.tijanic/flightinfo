package rs.atessoft.flights.mapper;

import rs.atessoft.flights.dto.FlightDto;
import rs.atessoft.flights.entity.Flight;

import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

public class FlightMapper {

	public static List<Flight> map(List<FlightDto> dtoList) {
		return dtoList.stream()
				.map(FlightMapper::map)
				.collect(Collectors.toList());
	}

	public static Flight map(FlightDto dto) {
		return Flight.builder()
				.flightId(dto.getFlightId())
				.arrivalAirportIATACode(dto.getArrivalAirportIATACode())
				.departureAirportIATACode(dto.getDepartureAirportIATACode())
				.departureDate(dto.getDepartureDate().atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime())
				.zoneOffset(dto.getDepartureDate().getOffset())
				.build();
	}

}
