package rs.atessoft.flights.entity;

import lombok.Data;
import rs.atessoft.flights.domain.CargoType;

import java.io.Serializable;

@Data
public class CargoId implements Serializable {

	private Long id;
	private Long flightId;
	private CargoType type;

}
