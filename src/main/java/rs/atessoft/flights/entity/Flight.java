package rs.atessoft.flights.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Flight {

	@Id
	private Long flightId;
	private int flightNumber;
	private String departureAirportIATACode;
	private String arrivalAirportIATACode;
	private LocalDateTime departureDate;
	private ZoneOffset zoneOffset;

}
