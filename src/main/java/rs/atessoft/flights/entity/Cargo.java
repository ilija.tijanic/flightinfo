package rs.atessoft.flights.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import rs.atessoft.flights.domain.CargoType;
import rs.atessoft.flights.domain.WeightUnit;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(CargoId.class)
public class Cargo implements Serializable {

	@Id
	private Long id;
	@Id
	private Long flightId;
	@Id
	@Enumerated(EnumType.STRING)
	private CargoType type;
	private Double weight;
	@Enumerated(EnumType.STRING)
	private WeightUnit weightUnit;
	private Double pieces;

}
