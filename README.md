## Smart4Aviation test app

### Requirements
* Java 11

### Running
* Run `mvn clean install` to run tests and build app
* Run `java -jar target/flights.jar` to start the app

### Links
* Swagger -> http://localhost:8080/swagger-ui/
* H2 console -> http://localhost:8080/h2-console